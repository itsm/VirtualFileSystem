﻿using Microsoft.Extensions.FileProviders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace VirtualFileSystem
{
    /// <summary>
    /// 虚拟文件系统扩展
    /// </summary>
    public static class VirtualFileSystemOptionExtensions 
    {
        /// <summary>
        /// 添加类所属程序集的嵌入式资源
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fileProviders"></param>
        /// <param name="requestPath">请求的根地址</param>
        /// <param name="baseNamespace">程序集默认命名空间</param>
        /// <returns></returns>
        public static List<IFileProvider> AddEmbeddedFile<T>(this List<IFileProvider> fileProviders, string? requestPath = null, string? baseNamespace = null)
        {
            AddEmbeddedFile(fileProviders, typeof(T).Assembly, requestPath, baseNamespace);
            return fileProviders;
        }

        /// <summary>
        /// 添加序集的嵌入式资源
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fileProviders"></param>
        /// <param name="assembly">程序集</param>
        /// <param name="requestPath">请求的根地址</param>
        /// <param name="baseNamespace">程序集默认命名空间</param>
        /// <returns></returns>
        public static List<IFileProvider> AddEmbeddedFile(this List<IFileProvider> fileProviders, Assembly assembly, string? requestPath = null, string? baseNamespace = null)
        {
            fileProviders.Add(new DictionaryEmbeddedFileProvider(assembly, requestPath, baseNamespace));
            return fileProviders;
        }


        /// <summary>
        /// 添加嵌入式资源
        /// </summary>
        /// <param name="fileProviders"></param>
        /// <param name="assemblies"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public static List<IFileProvider> AddEmbeddedFile(this List<IFileProvider> fileProviders, params Assembly[] assemblies)
        {
            if (assemblies == null || assemblies.Length == 0)
            {
                throw new ArgumentNullException(nameof(assemblies));
            }
            foreach (var assembly in assemblies)
            {
                fileProviders.Add(new DictionaryEmbeddedFileProvider(assembly));
            }
            return fileProviders;
        }

        /// <summary>
        /// 添加物理文件,保持最高优先级
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fileProviders"></param>
        /// <param name="contentPath"></param>
        /// <returns></returns>
        public static List<IFileProvider> AddPhysicalFile(this List<IFileProvider> fileProviders, string contentPath)
        {
            var insertIndex = 0;
            for (var i = 0; i < fileProviders.Count; i++)
            {
                if (fileProviders[i] is PhysicalFileProvider == false)
                {
                    insertIndex = i;
                    break;
                }
            }
            fileProviders.Insert(insertIndex, new PhysicalFileProvider(contentPath));
            return fileProviders;
        }

        /// <summary>
        /// 使用指定物理目录下的文件替换嵌入式资源
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fileProviders"></param>
        /// <param name="contentPath"></param>
        /// <returns></returns>
        public static List<IFileProvider> ReplaceEmbedToPhysical<T>(this List<IFileProvider> fileProviders, string contentPath)
        {
            for (var i = 0; i < fileProviders.Count; i++)
            {
                var fileProvider = fileProviders[i];
                if (fileProvider is DictionaryEmbeddedFileProvider embeddedFileProvider)
                {
                    if (embeddedFileProvider.Assembly == typeof(T).Assembly)
                    {
                        embeddedFileProvider.ReplaceEmbedToPhysical(contentPath);
                    }
                }
            }
            return fileProviders;
        }


        /// <summary>
        /// 使用项目默认地址的物理文件替换所有嵌入式资源
        /// </summary>
        /// <param name="fileProviders"></param>
        /// <param name="baseContentPath"></param>
        /// <returns></returns>
        public static List<IFileProvider> ReplaceAllEmbedToPhysical(this List<IFileProvider> fileProviders, string contentPath)
        {
            contentPath = contentPath.TrimEnd(Path.DirectorySeparatorChar);
            contentPath = contentPath[..contentPath.LastIndexOf(Path.DirectorySeparatorChar)];
            for (var i = 0; i < fileProviders.Count; i++)
            {
                var fileProvider = fileProviders[i];
                if (fileProvider is DictionaryEmbeddedFileProvider embeddedFileProvider)
                {
                    embeddedFileProvider.ReplaceEmbedToPhysical(Path.Combine(contentPath, embeddedFileProvider.BaseNamespace));
                }
            }
            return fileProviders;
        }
    }
}
