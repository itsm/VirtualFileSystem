﻿using Microsoft.Extensions.FileProviders;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualFileSystem
{
    public class VirtualDirectoryContents : IDirectoryContents
    {
        
        public bool Exists => true;

        IEnumerator<IFileInfo> fileList;

        public VirtualDirectoryContents(IEnumerator<IFileInfo> fileList)
        {
            this.fileList = fileList; 
        }


        public IEnumerator<IFileInfo> GetEnumerator()
        {
            return fileList;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return fileList;
        }
    }
}
