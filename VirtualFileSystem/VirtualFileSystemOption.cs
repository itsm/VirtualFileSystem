﻿using Microsoft.Extensions.FileProviders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace VirtualFileSystem
{
    public class VirtualFileSystemOption
    {
        
        /// <summary>
        /// 是否启用目录浏览
        /// </summary>
        public bool EnableDirectoryBrowser { get; set; }

        /// <summary>
        /// 文件提供器
        /// </summary>
        public List<IFileProvider> FileProviders { get; set; }


        public VirtualFileSystemOption()
        {
            FileProviders = new List<IFileProvider>();
        }
    }

   
}
