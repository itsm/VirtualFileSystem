﻿using Microsoft.Extensions.FileProviders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualFileSystem
{
    public class VirtualDirectoryFileInfo : IFileInfo
    {
        public bool Exists => true;

        public long Length => 0;

        public string PhysicalPath { get; }

        public string Name { get; }

        public DateTimeOffset LastModified { get; }

        public bool IsDirectory => true;

        public VirtualDirectoryFileInfo(string physicalPath, string name, DateTimeOffset lastModified)
        {
            PhysicalPath = physicalPath;
            Name = name;
            LastModified = lastModified;
        }

        public Stream CreateReadStream()
        {
            throw new InvalidOperationException();
        }
    }
}
