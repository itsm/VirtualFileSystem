﻿using Microsoft.Extensions.FileProviders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualFileSystem
{
    /// <summary>
    /// 物理文件信息
    /// </summary>
    public class VirtualPhysicalFileInfo : IFileInfo
    {
        public string FileName { get; set; }
        public VirtualPhysicalFileInfo(string fileName)
        {
            FileName = fileName;
        }
        public bool Exists => File.Exists(FileName);

        public bool IsDirectory => false; 

        public DateTimeOffset LastModified => File.GetLastWriteTimeUtc(FileName);

        public long Length => new FileInfo(FileName).Length;

        public string Name => Path.GetFileName(FileName);

        public string PhysicalPath => FileName;

        public Stream CreateReadStream()
        {
            var stream=File.Open(FileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            return stream;
        }
    }
}
