# VirtualFileSystem 虚拟文件系统

#### 介绍
虚拟文件系统，可用于支持Assembly中的嵌入式资源文件，可以代替自带的的UseStaticFiles及UseDirectoryBrowser，引入了EmbeddedFile的支持，可以将静态资源放在非webroot文件夹中，打包后静态资源也直接在dll中，没有多余的静态文件。

目前很多带界面的第三方组件库都是通过类似的方法实现的，
例如：swagger自带的管理界面,abpvnext中的虚拟文件系统


#### 使用说明

1.  引用VirtualFileSystem
2.  将各类数据文件(如：html,css,js)修改为"嵌入式资源"
3.  在web启动模板中调用:
````csharp
    app.UseVirtualFileSystem(option =>
    {
        option.FileProviders.AddEmbeddedFile<Program>();
    });
````
或
````csharp
    app.UseVirtualFileSystem(option =>
    {
        option.FileProviders.AddEmbeddedFile(typeof(Program).Assembly);
    });
````
4.  启动web项目即可通过地址栏即可访问嵌入式文件

#### 功能说明：

##### 物理文件：

开发过程中可使用物理文件替换掉嵌入式资源文件，方便实习调整文件内容，而不需要重新编译生成嵌入式资源文件。
以下示例，会采用默认解决方案的目录结构替换所有嵌入式资源文件

````csharp
    app.UseVirtualFileSystem(option =>
    {
        option.FileProviders.AddEmbeddedFile<Program>();
        if (builder.Environment.IsDevelopment())
        {
            option.FileProviders.ReplaceAllEmbedToPhysical(builder.Environment.ContentRootPath);
        }
    });
````
以下示例为单个Assembly指定目录地址替换嵌入式资源文件
````csharp
    app.UseVirtualFileSystem(option =>
    {
        option.FileProviders.AddEmbeddedFile<Program>();
        option.FileProviders.AddEmbeddedFile<VirtualFileSystem.Test.GetfilesTest>();
        if (builder.Environment.IsDevelopment())
        {
            option.FileProviders.ReplaceEmbedToPhysical<VirtualFileSystem.Test.GetfilesTest>(@"c:\src\VirtualFileSystem.Test);
        }
    });
````
##### 请求前缀地址

可为每个Assembly中的嵌入式资源指定请求前缀地址，默认虚拟文件的请求地址为根目录。
````csharp
    app.UseVirtualFileSystem(option =>
    {
        option.FileProviders.AddEmbeddedFile<Program>("VirtualFile"); //为指定请求前缀地址
    });
````
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
